import React from "react";
import Title from "../Title";
import aboutBcg from "../../images/aboutBcg.jpeg";

export default function Info() {
  return (
    <section className="py-5">
      <div className="container">
        <div className="row">
          <div className="col-10 mx-auto col-md-6 my-3">
            <img
              src={aboutBcg}
              alt="about"
              className="img-fluid img-thumbnail"
              style={{ background: "var(--darkGrey)" }}
            />
          </div>
          <div className="col-10 mx-auto col-md-6 my-3">
            <Title title="about us" />
            <p className="text-lead text-muted my-3">
              Knowledge is a familiarity, awareness, or understanding of someone
              or something, such as facts, information, descriptions, or skills,
              which is acquired through experience or education by perceiving,
              discovering, or learning.
            </p>
            <p className="text-lead text-muted my-3">
              Knowledge is a familiarity, awareness, or understanding of someone
              or something, such as facts, information, descriptions, or skills,
              which is acquired through experience or education by perceiving,
              discovering, or learning.
            </p>
            <button
              className="main-link"
              type="button"
              style={{ marginTop: "2rem" }}
            >
              more info
            </button>
          </div>
        </div>
      </div>
    </section>
  );
}
