import React from "react";
import Title from "../Title";
export default function Contact() {
  return (
    <section className="py-5" style={{ background: "#d2dfe0" }}>
      <div className="row">
        <div className="col-10 mx-auto col-md-6 my-3">
          <Title title="contact us" />
          <form
            className="my-5"
            action="https://formspree.io/simpledaniel.1818@gmail.com"
            method="POST"
          >
            {/*single element */}
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                name="firstName"
                placeholder="eg John Doe"
              />
            </div>
            {/*single element */}
            <div className="form-group">
              <input
                type="email"
                className="form-control"
                name="email"
                placeholder="eg doe@example.com"
              />
            </div>
            {/*single element */}
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                name="subject"
                placeholder="eg important!!!"
              />
            </div>
            {/* single message */}
            <div className="form">
              <textarea
                name="message"
                id=""
                rows="10"
                className="form-control"
                placeholder="ie. hello there buddy"
              ></textarea>
            </div>
            {/* submit */}
            <div className="form-group mt-3">
              <input
                type="submit"
                className="form-control bg-success text-light"
                value="Send"
              />
            </div>
          </form>
        </div>
      </div>
    </section>
  );
}
