import React from "react";
import {
  FaTrash,
  FaChevronCircleUp,
  FaChevronCircleDown
} from "react-icons/fa";
export default function CartItem({
  cartItem,
  increment,
  decrement,
  removeItem
}) {
  const { id, title, price, count, total, image } = cartItem;

  return (
    <div className="row mt-5 mt-lg-0 text-capitalize text-center align-item-center">
      {/* image div */}
      <div className="col-10 mx-auto col-lg-2 pb-2">
        <img src={image} width="50" className="img-fluid" alt="product" />
      </div>
      {/* product div */}
      <div className="col-10 mx-auto col-lg-2 pb-2">
        <span className="d-lg-none">product: </span>
        {title}
      </div>
      {/* price div */}
      <div className="col-10 mx-auto col-lg-2 pb-2">
        <span className="d-lg-none">price: kshs </span>
        {price}
      </div>
      {/* controls div */}
      <div className="col-10 mx-auto col-lg-2 my-2 my-lg-0">
        <div className="d-flex justify-content-center">
          <div>
            <FaChevronCircleDown
              onClick={() => decrement(id)}
              className="cart-icon text-primary"
            />
            <span className="text-title text-muted mx-3">{count}</span>
            <FaChevronCircleUp
              onClick={() => increment(id)}
              className="cart-icon text-primary"
            />
          </div>
        </div>
      </div>
      {/* removeItem div */}
      <div className="col-10 mx-auto col-lg-2">
        <FaTrash
          className="text-danger cart-icon"
          onClick={() => removeItem(id)}
        />
      </div>
      {/* cart total div */}
      <div className="col-10 mx-auto col-lg-2">
        <strong className="text-muted">item total : Kshs {total}</strong>
      </div>
    </div>
  );
}
